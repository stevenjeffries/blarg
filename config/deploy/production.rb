
set :rbenv_type, :user
set :rbenv_ruby, "2.5.3"

set :puma_threads, [4, 16]

set :deploy_to, "/home/deploy/apps/blarg"

set :stage, :production
set :rails_env, :production
set :branch, "master"

server "example.com", user: "deploy", roles: %w{app db web production}
