
set :rbenv_type, :user
set :rbenv_ruby, "2.5.3"

set :puma_threads, [4, 16]

set :deploy_to, "/home/deploy/apps/blarg"

set :stage, :local
set :rails_env, :local
set :branch, "development"

server "192.168.47.88", user: "deploy", roles: %w{app db web local}
