Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"

  config.vm.network "private_network", ip: "192.168.47.88"

  # Vagrant runs the provision scripts from the /tmp directory; this is placed there so that
  # the "root" provision script can find it.
  config.vm.provision "file", source: "bin/provision/deploy.sh", destination: "/tmp/deploy.sh"
  config.vm.provision "shell", path: "bin/provision/root.sh", args: "--generate-cert --app-name='blarg' --deploy-user='deploy' --ruby-version='2.5.3' --host-name='_'"
  config.vm.provision "shell", inline: "rm /tmp/deploy.sh"

  # Copies your public key to the deploy user on the virtual machine.
  public_key_file = File.join(Dir.home, ".ssh", "id_rsa.pub")
  if File.exist?(public_key_file)
    config.vm.provision "file", source: public_key_file, destination: "/tmp/host_public_key"
    config.vm.provision "shell", inline: %{
      sudo -u deploy mkdir -p /home/deploy/.ssh
      cat /tmp/host_public_key >> /home/deploy/.ssh/authorized_keys
      rm /tmp/host_public_key
      sudo -u deploy createdb blarg_local
    }
  end

  # Moves the cloudimg-console.log file to the log dir.
  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--uartmode1", "file", File.join(__dir__, "log", "ubuntu-cloudimg-console.log")]
  end
end
